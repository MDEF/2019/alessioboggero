
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>



#define DHTPIN 2     

#define DHTTYPE    DHT11     // DHT 11

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;


void setup() {
  Serial.begin(9600);
  dht.begin();
  Serial.println(F("DHTxx Unified Sensor Example"));
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  delayMS = sensor.min_delay / 1000;
}

void loop() {
  // Delay between measurements.
  delay(delayMS);
  pinMode (10 , OUTPUT);
  pinMode (11 , OUTPUT);
  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
    digitalWrite(10, HIGH);
    digitalWrite(11, HIGH);
    
   
    
  }
  else if ( event.temperature < 10 )  {
    Serial.print(F("Temperature: "));
    Serial.print(event.temperature);
    Serial.println(F("°C"));
    digitalWrite(10, HIGH);
    digitalWrite(11, LOW);
 
  }

  else if ( event.temperature > 22 )  {
    Serial.print(F("Temperature too high: "));
    Serial.print(event.temperature);
    Serial.println(F("°C"));
    digitalWrite(10, LOW);
    digitalWrite(11, HIGH);
   
   
  }

   else {
    Serial.print(F("Temperature: "));
    Serial.print(event.temperature);
    Serial.println(F("°C"));
    digitalWrite(10, LOW);
    digitalWrite(11, HIGH);
    
  }
}
